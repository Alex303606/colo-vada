$(document).ready(function () {
	
	//ViewPort 480px
	var viewport = document.createElement("meta");
	viewport.setAttribute("name", "viewport");
	if (screen.width < 480) {
		viewport.setAttribute("content", "width=480");
	} else {
		viewport.setAttribute("content", "width=device-width, initial-scale=1");
	}
	document.head.appendChild(viewport);
	
	$('#open-dropdown').on('click', function () {
		$(this).toggleClass('open');
		if($(this).hasClass('open')){
			$('.open-dropdown img').attr('src','img/polygon-up.svg');
		} else {
			$('.open-dropdown img').attr('src','img/polygon.svg');
		}
		$('.dropdown-menu').slideToggle();
	});
	
	$('.btn-menu').on('click',function () {
		$('.main-menu').fadeIn();
	});
	
	$('.close-menu').on('click',function () {
		$('.main-menu').fadeOut();
	});
	
	// swiper start
	if($(window).width() < 768 && !$('.swiper-container').hasClass('main-page-swiper')){
		var menuButton = document.querySelector('.menu-button');
		var swiper = new Swiper('.swiper-container', {
			slidesPerView: 'auto',
			initialSlide: 1,
			resistanceRatio: 0,
			slideToClickedSlide: true,
			on: {
				init: function () {
					var slider = this;
					menuButton.addEventListener('click', function () {
						if (slider.activeIndex === 0) {
							slider.slideNext();
						} else {
							slider.slidePrev();
						}
					}, true);
				},
				slideChange: function () {
					var slider = this;
					if (slider.activeIndex === 0) {
						menuButton.classList.add('cross');
					} else {
						menuButton.classList.remove('cross');
					}
				},
			}
		});
	}
	// swiper end
	
	$('.selectric').selectric();
	
	$('.open-dropdown a').on('click',function (e) {
		e.preventDefault();
		$(this).toggleClass('open');
		$(this).next('.dropdown').slideToggle();
	});
	
	$('.tabs').tabslet();
	
	// function marginTopSidebarMnu() {
	// 	var sMenu = $('.swiper-slide.menu');
	// 	var mTop = window.pageYOffset;
	// 	sMenu.css('margin-top', mTop);
	// }
	//
	// marginTopSidebarMnu();
	//
	// window.onscroll = function () {
	// 	marginTopSidebarMnu();
	// }
	
	$('#logIn').on('click',function (e) {
		e.preventDefault();
		$('#modalLogIn a i').remove();
		$('#modalLogIn').find('a.vk').prepend('<i class="fa fa-vk" aria-hidden="true"></i>');
		$('#modalLogIn').find('a.google').prepend('<i class="fa fa-google" aria-hidden="true"></i>');
		$('#modalLogIn').find('a.fb').prepend('<i class="fa fa-facebook" aria-hidden="true"></i>');
		$('#modalLogIn').find('a.instagram').prepend('<i class="fa fa-instagram" aria-hidden="true"></i>');
		$('#modalLogIn').show();
		$('body').append('<div id="wrapper"></div>');
	});
	
	$(document).on('click','#wrapper,#closeModal',function () {
		$('#wrapper').remove();
		$('#modalLogIn').hide();
		$('#selectCountry').hide();
	});
	
	$('#openSelectCountry').on('click',function(){
		$('#selectCountry').show();
		$('body').append('<div id="wrapper"></div>');
	});
	
	$('.slider-comments').slick({
		dots: true,
		autoplay: true,
		infinity: true,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					arrows: false
				}
			},
		]
	});
	
	var swiperMain = new Swiper('.main-page-swiper', {
		direction: 'vertical',
		slidesPerView: 1,
		spaceBetween: 0,
		mousewheel: true,
		pagination: {
			el: '.swiper-pagination',
			clickable: true
		},
		navigation: {
			nextEl: '.go_down'
		}
	});
	
	$('#connectSlider').slick({
		arrows: true,
		autoplay: true,
		infinity: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 970,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true
				}
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 580,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	
	if($(window).width() < 870) {
		$('.slide_3 .items').slick();
	}
	
	if($(window).width() < 1000) {
		$('.whats').slick();
	}
	
	//Anchors main menu
	
	function get(name){
		if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
			return decodeURIComponent(name[1]);
	}
	let active = get('active');
	if(active){
		swiperMain.slideTo(active, 1000, false);
	}
	
	const mainMenuLinks = $('.main-menu ul li a');
	for (let i = 0; i <= mainMenuLinks.length; i++) {
		$('#anchor' + i).on('click', function () {
			swiperMain.slideTo(i, 1000, false);
			$('.main-menu').fadeOut();
		});
	}
	
	
});



